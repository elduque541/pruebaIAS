import { Component, OnInit } from '@angular/core';
import { Ave } from './model/ave';
import { Zona } from './model/zona'
import { aveService } from './ave.service';
import { zonaService } from './zona.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pruebaIAS';

  ave: Ave = new Ave();
  aves: Ave[];

  zonas: Zona[];

  constructor(private aveService: aveService, private zonaService: zonaService) {
  }

  ngOnInit() {
    this.cargarAves();
    this.cargarZonas();
  }

  cargarAves(): void {
    this.aveService.getAves().subscribe(
      aves => this.aves = aves
    );
  }

  cargarZonas(): void {
    this.zonaService.getZonas().subscribe(
      zonas => this.zonas = zonas
    );
  }

  editar(ave: Ave) {
    this.ave = ave;
  }

  limpiar() {
    this.ave = new Ave();
  }

  crear(): void {
    this.aveService.create(this.ave).subscribe(
      response => {
        this.cargarAves()
      }
    )
    this.limpiar();
  }

  eliminar(ave: Ave): void {
    this.aveService.delete(ave.cdAve).subscribe(
      response => {
        this.cargarAves()
      }
    )
    this.limpiar();
  }

  public buscarCoincidencia(event: any): void {
    if (this.ave.filtrar != "") {
      this.aveService.getAvesFiltro(this.ave.filtrar).subscribe(
        aves => this.aves = aves
      );
    } else {
      this.cargarAves();
    }
  }

  public buscarPorZona(): void {
    if (this.ave.filtrarZona != "") {
      this.aveService.getAvesFiltroZona(this.ave.filtrarZona).subscribe(
        aves => this.aves = aves
      );
    } else {
      this.cargarAves();
    }
  }
}