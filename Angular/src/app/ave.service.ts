import { Injectable } from '@angular/core';
import { Ave } from './model/ave';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class aveService {
  private urlEndPoint: string = 'http://localhost:8080/api/aves';
  private urlEndPointFiltro: string = 'http://localhost:8080/api/aves/filtro';
  private urlEndPointFiltroZona: string = 'http://localhost:8080/api/aves/zona/';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getAves(): Observable<Ave[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Ave[])
    );
  }

  getAvesFiltro(cadena): Observable<Ave[]> {
    return this.http.get(`${this.urlEndPointFiltro}/${cadena}`).pipe(
      map(response => response as Ave[])
    );
  }

  getAvesFiltroZona(cadena): Observable<Ave[]> {
    return this.http.get(`${this.urlEndPointFiltroZona}/${cadena}`).pipe(
      map(response => response as Ave[])
    );
  }

  create(ave: Ave): Observable<Ave> {
    return this.http.post<Ave>(this.urlEndPoint, ave, { headers: this.httpHeaders })
  }

  getAve(id): Observable<Ave> {
    return this.http.get<Ave>(`${this.urlEndPoint}/${id}`)
  }

  update(ave: Ave): Observable<Ave> {
    return this.http.put<Ave>(`${this.urlEndPoint}/${ave.cdAve}`, ave, { headers: this.httpHeaders })
  }

  delete(id: String): Observable<Ave> {
    return this.http.delete<Ave>(`${this.urlEndPoint}/${id}`, { headers: this.httpHeaders })
  }

}
