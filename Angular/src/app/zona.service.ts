import { Injectable } from '@angular/core';
import { Zona } from './model/zona';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class zonaService {
  private urlEndPoint: string = 'http://localhost:8080/api/zonas';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getZonas(): Observable<Zona[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Zona[])
    );
  }

}
