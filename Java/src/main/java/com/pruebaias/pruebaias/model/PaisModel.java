package com.pruebaias.pruebaias.model;

public class PaisModel {

	private String cdPais;
	private String dsNombre;

	public String getCdPais() {
		return cdPais;
	}

	public void setCdPais(String cdPais) {
		this.cdPais = cdPais;
	}

	public String getDsNombre() {
		return dsNombre;
	}

	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}

	public PaisModel(String cdPais, String dsNombre) {
		super();
		this.cdPais = cdPais;
		this.dsNombre = dsNombre;
	}

	public PaisModel() {
	}

}
