package com.pruebaias.pruebaias.model;

public class AveModel {

	private String cdAve;
	private String dsNombreComun;
	private String dsNombreCientifico;

	public String getCdAve() {
		return cdAve;
	}

	public void setCdAve(String cdAve) {
		this.cdAve = cdAve;
	}

	public String getDsNombreComun() {
		return dsNombreComun;
	}

	public void setDsNombreComun(String dsNombreComun) {
		this.dsNombreComun = dsNombreComun;
	}

	public String getDsNombreCientifico() {
		return dsNombreCientifico;
	}

	public void setDsNombreCientifico(String dsNombreCientifico) {
		this.dsNombreCientifico = dsNombreCientifico;
	}

	public AveModel(String cdAve, String dsNombreComun, String dsNombreCientifico) {
		super();
		this.cdAve = cdAve;
		this.dsNombreComun = dsNombreComun;
		this.dsNombreCientifico = dsNombreCientifico;
	}

	public AveModel() {
	}

}
