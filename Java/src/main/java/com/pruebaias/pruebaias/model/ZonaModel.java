package com.pruebaias.pruebaias.model;

public class ZonaModel {

	private String cdZona;
	private String dsNombre;

	public String getCdZona() {
		return cdZona;
	}

	public void setCdZona(String cdZona) {
		this.cdZona = cdZona;
	}

	public String getDsNombre() {
		return dsNombre;
	}

	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}

	public ZonaModel(String cdZona, String dsNombre) {
		super();
		this.cdZona = cdZona;
		this.dsNombre = dsNombre;
	}

	public ZonaModel() {
	}

}
