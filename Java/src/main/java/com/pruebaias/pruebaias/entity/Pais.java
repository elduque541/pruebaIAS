package com.pruebaias.pruebaias.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TONT_PAISES")
public class Pais {

	@Id
	@Column(name = "CDPAIS", length = 3)
	private String cdPais;

	@Column(name = "DSNOMBRE", length = 100)
	private String dsNombre;

	public String getCdPais() {
		return cdPais;
	}

	public void setCdPais(String cdPais) {
		this.cdPais = cdPais;
	}

	public String getDsNombre() {
		return dsNombre;
	}

	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}

	public Pais(String cdPais, String dsNombre) {
		super();
		this.cdPais = cdPais;
		this.dsNombre = dsNombre;
	}

	public Pais() {
	}

	public Pais(String cdPais, String dsNombre, String cdzona) {
		super();
		this.cdPais = cdPais;
		this.dsNombre = dsNombre;
	}

}
