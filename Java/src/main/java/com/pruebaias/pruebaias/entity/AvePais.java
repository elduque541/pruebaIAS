package com.pruebaias.pruebaias.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "tont_aves_pais")
public class AvePais {

	@Id
	@NotEmpty
	@Column(name = "cdave")
	private String cdave;

	@NotEmpty
	@Column(name = "cdpais")
	private String cdpais;

	public String getCdave() {
		return cdave;
	}

	public void setCdave(String cdave) {
		this.cdave = cdave;
	}

	public String getCdpais() {
		return cdpais;
	}

	public void setCdpais(String cdpais) {
		this.cdpais = cdpais;
	}

}
