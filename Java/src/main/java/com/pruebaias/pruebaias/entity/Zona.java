package com.pruebaias.pruebaias.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TONT_ZONAS")
public class Zona {

	@Id
	@Column(name = "CDZONA", length = 3)
	private String cdZona;

	@Column(name = "DSNOMBRE", length = 45)
	private String dsNombre;

	public String getCdZona() {
		return cdZona;
	}

	public void setCdZona(String cdZona) {
		this.cdZona = cdZona;
	}

	public String getDsNombre() {
		return dsNombre;
	}

	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}

	public Zona(String cdZona, String dsNombre) {
		super();
		this.cdZona = cdZona;
		this.dsNombre = dsNombre;
	}

	public Zona() {
	}

}
