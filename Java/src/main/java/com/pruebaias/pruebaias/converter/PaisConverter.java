package com.pruebaias.pruebaias.converter;

import org.springframework.stereotype.Component;

import com.pruebaias.pruebaias.entity.Pais;
import com.pruebaias.pruebaias.model.PaisModel;

@Component("paisConverter")
public class PaisConverter {

	public Pais converterPaisToPaisModel(PaisModel paisModel) {
		return new Pais(paisModel.getCdPais(), paisModel.getDsNombre());
	}

	public PaisModel converterPaisToPaisModel(Pais pais) {
		return new PaisModel(pais.getCdPais(), pais.getDsNombre());
	}
}
