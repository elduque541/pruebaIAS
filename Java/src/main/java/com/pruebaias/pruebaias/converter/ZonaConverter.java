package com.pruebaias.pruebaias.converter;

import org.springframework.stereotype.Component;

import com.pruebaias.pruebaias.entity.Zona;
import com.pruebaias.pruebaias.model.ZonaModel;

@Component("zonaConverter")
public class ZonaConverter {

	public ZonaModel converterZonaToZonaModel(Zona zona) {
		return new ZonaModel(zona.getCdZona(), zona.getDsNombre());
	}

	public Zona converterZonaModelToZona(ZonaModel zona) {
		return new Zona(zona.getCdZona(), zona.getDsNombre());
	}
}
