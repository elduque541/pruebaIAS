package com.pruebaias.pruebaias.converter;

import org.springframework.stereotype.Component;

import com.pruebaias.pruebaias.entity.Ave;
import com.pruebaias.pruebaias.model.AveModel;

@Component("aveConverter")
public class AveConverter {

	public Ave convertAveModelToAve(AveModel aveModel) {
		return new Ave(aveModel.getCdAve(), aveModel.getDsNombreComun(), aveModel.getDsNombreCientifico());
	}

	public AveModel convertAveToAveModel(Ave ave) {
		return new AveModel(ave.getCdAve(), ave.getDsNombreComun(), ave.getDsNombreCientifico());
	}

}
