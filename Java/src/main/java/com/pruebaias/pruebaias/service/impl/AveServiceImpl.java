package com.pruebaias.pruebaias.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pruebaias.pruebaias.converter.AveConverter;
import com.pruebaias.pruebaias.entity.Ave;
import com.pruebaias.pruebaias.model.AveModel;
import com.pruebaias.pruebaias.repository.AvePaisRepository;
import com.pruebaias.pruebaias.repository.AveRepository;
import com.pruebaias.pruebaias.service.AveService;

@Service("aveServiceImpl")
public class AveServiceImpl implements AveService {

	@Autowired
	@Qualifier("aveRepository")
	private AveRepository aveRepository;

	@Autowired
	@Qualifier("avePaisRepository")
	private AvePaisRepository avePaisRepository;
	
	@Autowired
	@Qualifier("aveConverter")
	private AveConverter aveConverter;

	@Override
	public AveModel add(AveModel aveModel) {
		return aveConverter.convertAveToAveModel(aveRepository.save(aveConverter.convertAveModelToAve(aveModel)));
	}

	@Override
	public void delete(String id) {
		avePaisRepository.deleteById(id);
		aveRepository.deleteById(id);
	}

	@Override
	public Ave findById(String id) {
		return aveRepository.findById(id).orElse(null);
	}

	@Override
	public List<AveModel> findAll() {
		return listAveToAveModel(aveRepository.findAll());
	}

	@Override
	public List<Ave> findAllAve() {
		return (aveRepository.findAll());
	}

	private List<AveModel> listAveToAveModel(List<Ave> listaAve) {
		List<AveModel> listaAveModel = new ArrayList<>();
		for (Ave ave : listaAve) {
			listaAveModel.add(aveConverter.convertAveToAveModel(ave));
		}
		return listaAveModel;
	}

	@Override
	public List<AveModel> findByLike(String cadena) {
		return listAveToAveModel(aveRepository.findByLike(cadena, cadena));
	}

	@Override
	public List<AveModel> findByZona(String zona) {
		return listAveToAveModel(aveRepository.findByZona(zona));
	}

}
