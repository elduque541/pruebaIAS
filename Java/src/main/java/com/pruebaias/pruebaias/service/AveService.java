package com.pruebaias.pruebaias.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.pruebaias.pruebaias.entity.Ave;
import com.pruebaias.pruebaias.model.AveModel;

@Service
public interface AveService {

	 abstract AveModel add(AveModel aveModel);

	 abstract void delete(String id);

	 abstract Ave findById(String id);

	 abstract List<AveModel> findAll();

	 abstract List<Ave> findAllAve();

	abstract List<AveModel> findByLike(String cadena);

	abstract List<AveModel> findByZona(String zona);

}
