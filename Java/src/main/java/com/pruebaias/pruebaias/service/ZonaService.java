package com.pruebaias.pruebaias.service;

import java.util.List;

import com.pruebaias.pruebaias.model.ZonaModel;

public interface ZonaService {

	public abstract List<ZonaModel> findAll();
}
