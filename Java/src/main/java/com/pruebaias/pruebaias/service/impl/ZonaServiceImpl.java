package com.pruebaias.pruebaias.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pruebaias.pruebaias.converter.ZonaConverter;
import com.pruebaias.pruebaias.entity.Zona;
import com.pruebaias.pruebaias.model.ZonaModel;
import com.pruebaias.pruebaias.repository.ZonaRepository;
import com.pruebaias.pruebaias.service.ZonaService;

@Service("zonaServiceImpl")
public class ZonaServiceImpl implements ZonaService {

	@Autowired
	@Qualifier("zonaRepository")
	private ZonaRepository zonaRepository;

	@Autowired
	@Qualifier("zonaConverter")
	private ZonaConverter zonaConverter;

	@Override
	public List<ZonaModel> findAll() {
		return listZonaToZonaModel(zonaRepository.findAll());
	}

	private List<ZonaModel> listZonaToZonaModel(List<Zona> listaZona) {
		List<ZonaModel> listaZonaModel = new ArrayList<>();
		for (Zona zona : listaZona) {
			listaZonaModel.add(zonaConverter.converterZonaToZonaModel(zona));
		}
		return listaZonaModel;
	}

}
