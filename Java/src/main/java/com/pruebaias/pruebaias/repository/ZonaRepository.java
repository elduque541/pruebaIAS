package com.pruebaias.pruebaias.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pruebaias.pruebaias.entity.Zona;

@Repository("zonaRepository")
public interface ZonaRepository extends JpaRepository<Zona, Serializable> {

}
