package com.pruebaias.pruebaias.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pruebaias.pruebaias.entity.AvePais;

@Repository("avePaisRepository")
public interface AvePaisRepository extends JpaRepository<AvePais, Serializable> {

}
