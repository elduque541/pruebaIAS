package com.pruebaias.pruebaias.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pruebaias.pruebaias.entity.Ave;

@Repository("aveRepository")
public interface AveRepository extends JpaRepository<Ave, Serializable> {

	@Query("select c from Ave c where c.dsNombreComun like %?1% or c.dsNombreCientifico like %?2% ")
	List<Ave> findByLike(String dsNombreComun, String dsNombreCientifico);

	@Query(value = "SELECT \r\n" + "    ave.*\r\n" + "FROM\r\n" + "    tont_aves ave\r\n"
			+ "	INNER JOIN tont_aves_pais ap ON ap.cdave = ave.cdave\r\n"
			+ "	INNER JOIN tont_zonas zona ON zona.cdzona =  ap.cdpais\r\n" + "where\r\n"
			+ "	zona.dsnombre = ?1 ", nativeQuery = true)
	List<Ave> findByZona(String zona);

}
