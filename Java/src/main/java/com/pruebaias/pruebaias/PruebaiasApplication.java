package com.pruebaias.pruebaias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaiasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaiasApplication.class, args);
	}

}

