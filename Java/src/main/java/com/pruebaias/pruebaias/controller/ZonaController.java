package com.pruebaias.pruebaias.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebaias.pruebaias.model.ZonaModel;
import com.pruebaias.pruebaias.service.ZonaService;

@RestController
@CrossOrigin(origins = { "http://localhost:4200" })
@RequestMapping("/api")
public class ZonaController {

	@Autowired
	@Qualifier("zonaServiceImpl")
	private ZonaService zonaService;

	@GetMapping("/zonas")
	public List<ZonaModel> listar() {
		return zonaService.findAll();
	}

}
