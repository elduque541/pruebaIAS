package com.pruebaias.pruebaias.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebaias.pruebaias.converter.AveConverter;
import com.pruebaias.pruebaias.entity.Ave;
import com.pruebaias.pruebaias.model.AveModel;
import com.pruebaias.pruebaias.service.AveService;

@RestController
@CrossOrigin(origins = { "http://localhost:4200" })
@RequestMapping("/api")
public class AveController {

	@Autowired
	@Qualifier("aveServiceImpl")
	private AveService aveService;

	@Autowired
	@Qualifier("aveConverter")
	private AveConverter aveConverter;

	@GetMapping("/aves")
	public List<AveModel> listar() {
		return aveService.findAll();
	}

	@GetMapping("/avesEntity")
	public List<Ave> listarEntity() {
		List<Ave> aves = aveService.findAllAve();
		return aves;
	}

	@GetMapping("/aves/filtro/{cadena}")
	public List<AveModel> listar(@PathVariable String cadena) {
		return aveService.findByLike(cadena);
	}

	@GetMapping("/aves/zona/{zona}")
	public List<AveModel> listarZona(@PathVariable String zona) {
		return aveService.findByZona(zona);
	}

	@GetMapping("/aves/{id}")
	public AveModel finById(@PathVariable String id) {
		return aveConverter.convertAveToAveModel(aveService.findById(id));
	}

	@PostMapping("/aves")
	public AveModel agregar(@RequestBody AveModel aveModel) {
		return aveService.add(aveModel);
	}

	@PutMapping("/aves/{id}")
	public AveModel actualizar(@RequestBody AveModel aveModel) {
		return aveService.add(aveModel);
	}

	@DeleteMapping("/aves/{id}")
	public List<AveModel> delete(@PathVariable String id) {
		aveService.delete(id);
		return aveService.findAll();
	}

}
